== Лекция 2. Правовые, нормативно-технические и организационные основы бжд

=== Конституционные основы обеспечения БЖ.

В Конституции РФ (##"`Права и свободы человека и гражданина`"##). Ряд статей
напрямую указывает на ценность жизни человека ##"`Человек, его права и
свободы являются высшей ценностью`"## (ст. 2), ##"`Каждый имеет право на
жизнь`"## (ст. 20).

В РФ сохраняются труд и здоровье людей (ст. 7), причем условие труда
должны отвечать требованиям безопасности и гигиены (ст. 37). Каждый
гражданин имеет право на отдых, так как жизнедеятельность - это
повседневная деятельность и отдых человека.

Население РФ имеет право на охрану здоровья и медицинскую помощь, что
осуществляется на основании реализации федеральных программ охраны и
укрепления здоровья человека (ст. 41).

*_Земельные ресурсы и окружающая природная среда_* используются и
охраняются в РФ как основа жизни и деятельности народов, проживающих на
соответствующей территории (ст. 9). Владение ими не должно наносить
ущерба окружающей среде и нарушать прав и законных интересов иных лиц
(ст. 36)

Каждый человек имеет право на: - благоприятную окружающею среду -
достоверную информацию о ее состоянии - на возмущение ущерба
причиненного его здоровью или имуществу экологическим правонарушением
(ст. 42)

=== Основной закон обеспечения БЖ в мирное время

ФЗ "`О защите населения и территорий от ЧС природного и техногенного
характера`"

Целями настоящего ФЗ являются: - предупреждение возникновения и развития
ЧС - снижение размеров ущерба и потерь от ЧС - ликвидация ЧС -
разграничение полномочий в области защиты населения и территорий от ЧС
органами исполнительной власти разных уровней

В законе дается определение Единой государственной систему
предупреждения и ликвидации ЧС (именуемой также - РСЧС), которая
объединяет органы управления, силы и средства федеральных органов
исполнительной власти, органов исполнительной власти субъектов РФ,
органов местного самоуправления, организаций, в полномочия которых
входит решение вопросов по защите населения и территорий от ЧС.

_Основными задачами единой государственной системы предупреждения и
ликвидации чрезвычайных ситуаций являются:_ - разработка и реализация
правовых и экономических норм; - осуществление целевых и
научно-технических программ; - обеспечение готовности к действия органов
управления, сил и средств, - предназначенных и выделяемых для
предупреждения и ликвидации чрезвычайных ситуаций; - сбор, обработка,
обмен и выдача информации; - подготовка населения к действиям; -
прогнозирование и оценка социально-экономических последствий - создание
резервов финансовых и материальных ресурсов - осуществление
государственной экспертизы, надзора и контроля в области защиты
населения и территорий - ликвидация ЧС - осуществление мероприятий по
социальной защите населения - реализация прав и обязанностей населения в
области защиты от ЧС - международное сотрудничество в области защиты
населения и территорий

_РСЧС включает в себя подсистемы:_ - Территориальную - Функциональную

_Территориальная включает уровни:_ - федеральный - региональный -
территориальный - местный - объектовый

Режимы функционирования РСЧС: - повседневный - повышенной готовности -
чрезвычайной ситуации

_Правительство РФ:_ - издает постановления и распоряжения в области
защиты населения и территорий - организует проведение научных
исследований - организует разработку специальных программ - осуществляет
руководство РСЧС - обеспечивает создание резервов финансовых и
материальных ресурсов - устанавливает контроль над радиоактивными и
другими особо опасными веществами - устанавливает классификацию ЧС -
обеспечивает защиту населения и территорий от ЧС федерального характера
- определяет порядок привлечения Войск гражданской обороны - определяет
порядок сбора информации и обмена информацией

Граждане Российской Федерации имеют право: - на защиту жизни, здоровья и
личного имущества; - использовать средства защиты; быть информированными
о риске, которому они могут подвергнуться; - обращаться лично в
государственные органы и органы местного самоуправления; - участвовать в
мероприятиях по предупреждению и ликвидации чрезвычайных ситуаций; - на
возмещение ущерба, причинённого их здоровью и имуществу; - на
медицинское обслуживание, компенсации и социальные гарантии за
проживание и работу в зонах чрезвычайных ситуаций; - на пенсионное
обеспечение по случаю потери кормильца.

Граждане Российской Федерации обязаны: - соблюдать законы и иные
нормативные правовые акты; - соблюдать меры безопасности в быту и
трудовой деятельности; - изучать способы защиты от чрезвычайных
ситуаций, приёмы оказания первой медицинской помощи пострадавшим; -
выполнять правила поведения при угрозе и возникновении чрезвычайных
ситуаций; - при необходимости оказывать содействие в проведении
аварийно-спасательных и других неотложных работ.

=== Другие законодательные акты, обеспечивающие безопасность жизнедеятельности

Отдельные законодательные акты направлены на защиту окружающей природной
среды (Лесной кодекс, Водный кодекс, Земельный кодекс).
Градостроительный кодекс определяет условия безопасности в сфере
строительства.

В Кодексе Российской Федерации «Об административных правонарушениях»
определена административная ответственность в случае нарушения
санитарно-эпидемиологического благополучия населения.

Отдельные законодательные акты направлены на обеспечение безопасности,
суверенитета и территориальной целостности нашего государства
(Федеральный Конституционный закон «О военном положении», Федеральный
закон «Об обороне»).

Важную составляющую формируют законы, направление на обеспечение
безопасности в техногенной сфере (Федеральный закон «О радиационной
безопасности населения», Федеральный закон «О пожарной безопасности»,
Федеральный закон «О промышленной безопасности опасных производственных
объектов», Федеральный закон "`О безопасности гидротехнических
Сооружений`", Федеральный закон «Технический регламент о безопасности
зданий сооружений»).

Следует обозначить документы, направленные на борьбу с терроризмом:
Федеральный закон «О противодействии терроризму», Указ Президента
Российской Федерации «О мерах по противодействию терроризму».

=== Гражданская оборона

image::2024-02-06T11-21-38-028Z.png[] 

Гражданская оборона (ГО) представляет собой систему мероприятий по
подготовке к защите и по защите населения, материальных и культурных
ценностей на территории Российской Федерации от опасностей, возникающих
при ведении военных действий или вследствие этих действий, а также при
возникновении чрезвычайных ситуаций природного и техногенного характера.
Система гражданской обороны строится по территориально-производственному
принципу

_Задачи ГО изложены в Федеральном законе Российской Федерации «О
гражданской обороне»:_ - обучение населения в области гражданской
обороны; - оповещение населения об опасностях; - эвакуация населения,
материальных и культурных ценностей; - предоставление населению убежищ и
средств защиты; - проведение мероприятий по маскировке; - проведение
аварийно-спасательных работ в случае военных действий и чрезвычайных
ситуаций; - обеспечение населения, пострадавшего при ведении военных
действий; - борьба с пожарами; - обнаружение и обозначение районов
радиоактивного, химического, биологического и иного заражения; -
санитарная обработка населения, обеззараживание зданий и сооружений,
специальная обработка техники и территорий; - восстановление и
поддержание порядка в районах; - срочное восстановление функционирования
необходимых коммунальных служб в военное время; - срочное захоронение
трупов в военное время; - осуществление мер, направленных на сохранение
объектов, необходимых для устойчивого функционирования экономики; -
обеспечение постоянной готовности сил и средств гражданской обороны.

